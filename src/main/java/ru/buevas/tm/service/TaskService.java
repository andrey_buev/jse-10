package ru.buevas.tm.service;

import java.util.List;
import ru.buevas.tm.entity.Task;
import ru.buevas.tm.repository.TaskRepository;

/**
 * Сервис проектов
 *
 * Делегирует операции над задачами репозиторию с предшествующей обработкой и валидацией поступающей информации
 *
 * @author Andrey Buev
 * @see TaskRepository
 */
public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        return taskRepository.create(name, description);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        return taskRepository.update(id, name, description);
    }

    public Task findByIndex(final int index) {
        return taskRepository.findByIndex(index);
    }

    public Task findById(final Long id) {
        return taskRepository.findById(id);
    }

    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public Task removeByIndex(final int index) {
        return taskRepository.removeByIndex(index);
    }

    public Task removeById(final Long id) {
        return taskRepository.removeById(id);
    }

    public Task removeByName(final String name) {
        return taskRepository.removeByName(name);
    }

    public void clear() {
        taskRepository.clear();
    }
}
