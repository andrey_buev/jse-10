package ru.buevas.tm.service;

import java.util.Collections;
import java.util.List;
import ru.buevas.tm.entity.Project;
import ru.buevas.tm.entity.Task;
import ru.buevas.tm.repository.ProjectRepository;
import ru.buevas.tm.repository.TaskRepository;

/**
 * Сервис для связывания задачи с проектом
 *
 * @author Andrey Buev
 */
public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(final ProjectRepository projectRepository, final TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Создание связи между задачей и проектом
     *
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return привязанная задача
     */
    public Task addTaskToProject(final Long projectId, final Long taskId) {
        if (projectId == null || taskId == null) return null;

        final Project project = projectRepository.findById(projectId);
        final Task task = taskRepository.findById(taskId);
        if (project == null || task == null) return null;

        task.setProjectId(projectId);
        return task;
    }

    /**
     * Поиск всех задач связанных с заданым проектом
     *
     * @param projectId идентификатор проекта
     * @return коллекция задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    /**
     * Удаление связи задачи с проектом
     *
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return отвязанная задача
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        if (projectId == null || taskId == null) return null;

        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;

        task.setProjectId(null);
        return task;
    }
}
