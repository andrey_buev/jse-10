package ru.buevas.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ru.buevas.tm.constant.TerminalConst.CliCommands;
import ru.buevas.tm.controller.ProjectController;
import ru.buevas.tm.controller.SystemController;
import ru.buevas.tm.controller.TaskController;
import ru.buevas.tm.repository.ProjectRepository;
import ru.buevas.tm.repository.TaskRepository;
import ru.buevas.tm.service.ProjectService;
import ru.buevas.tm.service.ProjectTaskService;
import ru.buevas.tm.service.TaskService;

/**
 * Основной класс приложения
 *
 * @author Andrey Buev
 */
public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final SystemController systemController = new SystemController();

    /**
     * Точка входа в приложение
     *
     * @param args параметры командной строки, переданные при старте приложения
     */
    public static void main(String[] args) {
        final Application application = new Application();
        application.systemController.printWelcome();
        application.run(args);
        application.process();
    }

    /**
     * Запуск приложения с аргументом из командной строки
     *
     * @param args массив аргументов
     */
    public void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String command = args[0];
        try {
            systemController.exit(execCommand(command));
        } catch (IOException | NumberFormatException e) {
            systemController.printError(e.getMessage());
        }
    }

    /**
     * Запуск приложения в режиме бесконечного цикла
     */
    public void process() {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String command = "";
        while (!CliCommands.EXIT.equals(command)) {
            try {
                command = reader.readLine();
                execCommand(command);
                System.out.println();
            } catch (IOException | NumberFormatException e) {
                systemController.printError(e.getMessage());
            }
        }
    }

    /**
     * Выполнение команды
     *
     * @param command выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */
    public int execCommand(final String command) throws IOException{
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case CliCommands.VERSION:
                systemController.printVersion();
                break;
            case CliCommands.ABOUT:
                systemController.printAbout();
                break;
            case CliCommands.HELP:
                systemController.printHelp();
                break;
            case CliCommands.EXIT:
                systemController.exit(0);
                break;

            case CliCommands.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CliCommands.PROJECT_LIST:
                projectController.listProject();
                break;
            case CliCommands.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CliCommands.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CliCommands.PROJECT_VIEW_BY_INDEX:
                projectController.viewProjectByIndex();
                break;
            case CliCommands.PROJECT_VIEW_BY_ID:
                projectController.viewProjectById();
                break;
            case CliCommands.PROJECT_VIEW_BY_NAME:
                projectController.viewProjectByName();
                break;
            case CliCommands.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CliCommands.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CliCommands.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case CliCommands.PROJECT_CLEAR:
                projectController.clearProject();
                break;

            case CliCommands.TASK_CREATE:
                taskController.createTask();
                break;
            case CliCommands.TASK_LIST:
                taskController.listTask();
                break;
            case CliCommands.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CliCommands.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CliCommands.TASK_VIEW_BY_INDEX:
                taskController.viewTaskByIndex();
                break;
            case CliCommands.TASK_VIEW_BY_ID:
                taskController.viewTaskById();
                break;
            case CliCommands.TASK_VIEW_BY_NAME:
                taskController.viewTaskByName();
                break;
            case CliCommands.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CliCommands.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CliCommands.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case CliCommands.TASK_ADD_TO_PROJECT_BY_IDS:
                taskController.addTaskToProjectByIds();
                break;
            case CliCommands.TASK_LIST_BY_PROJECT_ID:
                taskController.listTaskByProjectId();
                break;
            case CliCommands.TASK_REMOVE_FROM_PROJECT_BY_IDS:
                taskController.removeTaskFromProjectByIds();
                break;
            case CliCommands.TASK_CLEAR:
                taskController.clearTask();
                break;

            default:
                systemController.printError("Unknown parameter");
                break;
        }
        return 0;
    }
}
