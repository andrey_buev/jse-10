package ru.buevas.tm.controller;

import java.io.IOException;
import java.util.List;
import ru.buevas.tm.entity.Task;
import ru.buevas.tm.service.ProjectTaskService;
import ru.buevas.tm.service.TaskService;

/**
 * Контроллер для операций над задачами
 *
 * @author Andrey Buev
 */
public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    /**
     * Обработка команды создания задачи
     */
    public void createTask() throws IOException {
        System.out.println("[CREATE TASK]");

        System.out.print("Enter task name: ");
        final String name = reader.readLine();

        System.out.print("Enter task description: ");
        final String description = reader.readLine();

        taskService.create(name, description);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды вывода списка задач
     */
    public void listTask() {
        System.out.println("[LIST TASK]");
        System.out.println("Available tasks:");
        final List<Task> tasks = taskService.findAll();
        printTaskList(tasks);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды обновления задачи по номеру в списке
     */
    public void updateTaskByIndex() throws IOException {
        System.out.println("[UPDATE TASK]");

        System.out.print("Enter task index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) printError("Task not found");

        System.out.print("Enter task name: ");
        final String name = reader.readLine();

        System.out.print("Enter task description: ");
        final String description = reader.readLine();

        taskService.update(task.getId(), name, description);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды обновления задачи по номеру в списке
     */
    public void updateTaskById() throws IOException {
        System.out.println("[UPDATE TASK]");

        System.out.print("Enter task id: ");
        final Long id = Long.parseLong(reader.readLine());
        final Task task = taskService.findById(id);
        if (task == null) printError("Task not found");

        System.out.print("Enter task name: ");
        final String name = reader.readLine();

        System.out.print("Enter task description: ");
        final String description = reader.readLine();

        taskService.update(task.getId(), name, description);
        System.out.println("[SUCCESS]");
    }

    /**
     * Вывод на экран информации о задаче
     *
     * @param task задача
     * @see Task
     */
    private void printTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("id: " + task.getId());
        System.out.println("name: " + task.getName());
        System.out.println("description: " + task.getDescription());
        System.out.println("[SUCCESS]");
    }

    /**
     * Вывод на экран коллекции задач
     *
     * @param tasks коллекция задач
     * @see Task
     */
    private void printTaskList(final List<Task> tasks) {
        if (tasks == null) return;
        if (tasks.isEmpty()) {
            System.out.println("Empty");
        } else {
            int index = 1;
            for (Task task : tasks) {
                System.out.printf("%d. %s - %s%n", index, task.getId(), task.getName());
                index++;
            }
        }
    }

    /**
     * Обработка команды отображения задачи по номеру в списке
     */
    public void viewTaskByIndex() throws IOException {
        System.out.print("Enter index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (task != null) printTask(task);
    }

    /**
     * Обработка команды отображения задачи по идентификатору
     */
    public void viewTaskById() throws IOException {
        System.out.print("Enter id: ");
        final Long id = Long.parseLong(reader.readLine());
        final Task task = taskService.findById(id);
        if (task != null) printTask(task);
    }

    /**
     * Обработка команды отображения задачи по имени
     */
    public void viewTaskByName() throws IOException {
        System.out.print("Enter name: ");
        final String name = reader.readLine();
        final Task task = taskService.findByName(name);
        if (task != null) printTask(task);
    }

    /**
     * Обработка команды удаления проекта по номеру в списке
     */
    public void removeTaskByIndex() throws IOException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final int index = Integer.parseInt(reader.readLine()) - 1;
        final Task task = taskService.removeByIndex(index);
        if (task != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed task not found");
        }
    }

    /**
     * Обработка команды удаления проекта по идентификатору
     */
    public void removeTaskById() throws IOException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("Enter id: ");
        final Long id = Long.parseLong(reader.readLine());
        final Task task = taskService.removeById(id);
        if (task != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed task not found");
        }
    }

    /**
     * Обработка команды удаления проекта по имени
     */
    public void removeTaskByName() throws IOException {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.print("Enter name: ");
        final String name = reader.readLine();
        final Task task = taskService.removeByName(name);
        if (task != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Removed task not found");
        }
    }

    /**
     * Обработка команды добавления задачи к проекту
     */
    public void addTaskToProjectByIds() throws IOException {
        System.out.println("[ADD TASK TO PROJECT BY ID]");
        System.out.print("Enter project id: ");
        final Long projectId = Long.parseLong(reader.readLine());
        System.out.print("Enter task id: ");
        final Long taskId = Long.parseLong(reader.readLine());

        final Task task = projectTaskService.addTaskToProject(projectId, taskId);
        if (task != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Error during adding task to project");
        }
    }

    /**
     * Обработка команды вывода на экран списка задач в проекте
     */
    public void listTaskByProjectId() throws IOException {
        System.out.println("[LIST TASK BY PROJECT ID]");
        System.out.print("Enter project id: ");
        final Long projectId = Long.parseLong(reader.readLine());

        final List<Task> tasks = projectTaskService.findAllByProjectId(projectId);
        printTaskList(tasks);
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды удаления задачи из проекта
     */
    public void removeTaskFromProjectByIds() throws IOException {
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        System.out.print("Enter project id: ");
        final Long projectId = Long.parseLong(reader.readLine());
        System.out.print("Enter task id: ");
        final Long taskId = Long.parseLong(reader.readLine());

        final Task task = projectTaskService.removeTaskFromProject(projectId, taskId);
        if (task != null) {
            System.out.println("[SUCCESS]");
        } else {
            printError("Error during removing task from project");
        }
    }

    /**
     * Обработка команды отчистки списка задач
     */
    public void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[SUCCESS]");
    }
}
