package ru.buevas.tm.controller;

import ru.buevas.tm.constant.TerminalConst.CliCommands;

/**
 * Контроллер для общесистемных команд
 *
 * @author Andrey Buev
 */
public class SystemController extends AbstractController {

    /**
     * Вывод на экран приветствия
     */
    public void printWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Обработка команды вывода версии приложения
     */
    public void printVersion() {
        System.out.println("Task Manager version 1.0.0");
    }

    /**
     * Обработка команды вывода сведений о разработчике
     */
    public void printAbout() {
        System.out.println("Developer: Andrey Buev (buev_as@mail.ru)");
    }

    /**
     * Обработка команды вывода справки
     */
    public void printHelp() {
        System.out.printf("%s - Display version information%n", CliCommands.VERSION);
        System.out.printf("%s - Display version information%n", CliCommands.ABOUT);
        System.out.printf("%s - Display version information%n", CliCommands.HELP);
        System.out.printf("%s - Display version information%n", CliCommands.EXIT);
        System.out.println();
        System.out.printf("%s - Create new project%n", CliCommands.PROJECT_CREATE);
        System.out.printf("%s - Display list of projects%n", CliCommands.PROJECT_LIST);
        System.out.printf("%s - Update project by index in list%n", CliCommands.PROJECT_UPDATE_BY_INDEX);
        System.out.printf("%s - Update project by id%n", CliCommands.PROJECT_UPDATE_BY_ID);
        System.out.printf("%s - View project by index in list%n", CliCommands.PROJECT_VIEW_BY_INDEX);
        System.out.printf("%s - View project by id%n", CliCommands.PROJECT_VIEW_BY_ID);
        System.out.printf("%s - View project by name%n", CliCommands.PROJECT_VIEW_BY_NAME);
        System.out.printf("%s - Remove project by index in list%n", CliCommands.PROJECT_REMOVE_BY_INDEX);
        System.out.printf("%s - Remove project by id%n", CliCommands.PROJECT_REMOVE_BY_ID);
        System.out.printf("%s - Remove project by name%n", CliCommands.PROJECT_REMOVE_BY_NAME);
        System.out.printf("%s - Clear list of projects%n", CliCommands.PROJECT_CLEAR);
        System.out.println();
        System.out.printf("%s - Create new task%n", CliCommands.TASK_CREATE);
        System.out.printf("%s - Display list of tasks%n", CliCommands.TASK_LIST);
        System.out.printf("%s - Update task by index in list%n", CliCommands.TASK_UPDATE_BY_INDEX);
        System.out.printf("%s - Update task by id%n", CliCommands.TASK_UPDATE_BY_ID);
        System.out.printf("%s - View task by index in list%n", CliCommands.TASK_VIEW_BY_INDEX);
        System.out.printf("%s - View task by id%n", CliCommands.TASK_VIEW_BY_ID);
        System.out.printf("%s - View task by name%n", CliCommands.TASK_VIEW_BY_NAME);
        System.out.printf("%s - Remove task by index in list%n", CliCommands.TASK_REMOVE_BY_INDEX);
        System.out.printf("%s - Remove task by id%n", CliCommands.TASK_REMOVE_BY_ID);
        System.out.printf("%s - Remove task by name%n", CliCommands.TASK_REMOVE_BY_NAME);
        System.out.printf("%s - Add task to project by ids%n", CliCommands.TASK_ADD_TO_PROJECT_BY_IDS);
        System.out.printf("%s - List task by project id%n", CliCommands.TASK_LIST_BY_PROJECT_ID);
        System.out.printf("%s - Remove task from project by ids%n", CliCommands.TASK_REMOVE_FROM_PROJECT_BY_IDS);
        System.out.printf("%s - Clear list of tasks%n", CliCommands.TASK_CLEAR);
    }
}
